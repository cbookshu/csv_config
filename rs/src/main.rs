use std::collections::HashMap;
use std::fs::File;
use std::io::{BufRead, BufReader};
use std::vec;

struct CsvConfig {
    cur_row:usize,
    cur_col:usize,
}

#[derive(Debug)]
enum Var {
    None,
    Num(f64),
    Str(String),
    Map(HashMap<String, Var>),
    List(Vec<Var>)
}

struct RangeCheck {
    lcose:bool,
    rclose:bool,
    l:f64,
    r:f64
}

impl RangeCheck {
    fn new() ->RangeCheck  {
        RangeCheck{
            lcose:false,
            rclose:false,
            l:0.0,
            r:0.0
        }
    }

    fn check(&self, v:f64) -> i32 {
        if v < self.l {
            return  -1;
        }

        if self.lcose  && v == self.l {
            return  -1;
        }

        if v > self.r {
            return  1;
        }

        if self.rclose && v == self.r {
            return  1;
        }

        return 0;
    }
}

struct Title {
    propertys: HashMap<String,String>,
    range:Option<RangeCheck>,
    must_value:bool,
    global:bool,
    grouparr:String,
    group:String,
    arr:bool
}

impl Title {
    fn new() -> Title {
        Title {
            propertys:HashMap::new(),
            range:Option::None,
            must_value:true,
            global:false,
            grouparr:String::new(),
            group:String::new(),
            arr:false,
        }
    }
}

impl CsvConfig {
    fn new()->CsvConfig{
        CsvConfig {
            cur_row:0,
            cur_col:0,
        }
    }

    fn parse(&mut self, path: &str) -> Result<Var, String> {
        let result = match self.csv_parse(path) {
            Ok(res) => res,
            Err(s) => {
                return Err(s);
            }
        };
        let mut globals = Var::Map(HashMap::new());
        let mut cur_data = Option::None;
        let mut cur_titlename = String::new();
        let mut res = Var::Map(HashMap::new());
        let mut cur_titles = Vec::new();

        for (i, line) in result.iter().enumerate() {
            self.cur_row = i + 1;

            let title_name = match line.first() {
                Some(r) => r.clone(),
                None => {
                    continue;
                }
            };

            if !title_name.is_empty() {
                println!("new tilte{}", title_name);

                if !cur_titlename.is_empty() {
                    if let Some(r) = cur_data {
                        if let Var::Map(m) = &mut res {
                            m.insert(cur_titlename, r);
                        }
                    }
                }

                cur_titles = match self.parse_title(line) {
                    Ok(ts) => ts,
                    Err(s) => {
                        return  Err(s);
                    }
                };
                cur_titlename = title_name;
                cur_data = Option::Some(Var::List(Vec::new()));
            } else {
                if !cur_titles.is_empty() {
                    match self.parse_data_line(&cur_titles, line, &mut globals) {
                        Ok(r) => {
                            if let Var::Map(m) = &r {
                                if !m.is_empty() && cur_data.is_some() {
                                    if let Some(data) = &mut cur_data {
                                        if let Var::List(l) = data {
                                            l.push(r);
                                        }
                                    }
                                }
                            }
                        },
                        Err(s) => {
                            return  Err(s);
                        }
                    };
                }
            }

        }

        if !cur_titlename.is_empty() {
            if let Var::Map(m) = &mut res {
                if let Some(data) = cur_data {
                    m.insert(cur_titlename, data);
                }
            }
        }

        if let Var::Map(m) = &mut globals {
            for (k,v) in m.iter_mut() {
                if let Var::Map(m) = &mut res {
                    m.insert(k.clone(), std::mem::replace(v, Var::None));
                }
            }
        }

        Ok(res)
    }

    fn parse_data_line(&mut self, titles:&Vec<Title>, line:&Vec<String>, globals:&mut Var) -> Result<Var, String> {

        let mut groups = HashMap::new();
        let mut grouparrs = HashMap::new();
        let mut err = String::new();
        let mut item = Var::Map(HashMap::new());
        for (i,v) in line.iter().enumerate() {
            self.cur_col = i;
            if titles.len() <= i {
                break;
            }

            let t = &titles[i];
            let key = if let Some(value) = t.propertys.get("key") {
                value
            } else {
                continue;
            };
            let mut groupname = String::new();
            let mut group_map = Option::<&mut HashMap<String,Var>>::None;
            if !t.grouparr.is_empty() {
                groupname = t.grouparr.clone();
                group_map = Some(grouparrs.entry(t.grouparr.clone()).or_insert(HashMap::<String,Var>::new()));
            } else if !t.group.is_empty() {
                groupname = t.group.clone();
                group_map = Some(groups.entry(t.group.clone()).or_insert(HashMap::<String,Var>::new()));
            }

            if v.is_empty() {
                if t.must_value {
                    err = format!("key:{}, row:{} col:{} value empty", key, self.cur_row, self.cur_col);
                }
                continue;
            }

            if !err.is_empty() {
                return Err(err);
            }

            let type_ = if let Some(type_str) = t.propertys.get("type") {
                type_str
            } else {
                // 不可能的，但是为了让rust的编译器满意，不增加unwrap只能这样了
                continue;
            };

            let mut _value = Option::<Var>::None;
            match type_.as_str() {
                "num" => {
                    let range = if let Some(range_t) = &t.range {
                        range_t
                    } else {
                        return Err(format!("key:{}, row:{} col:{} range empty", key, self.cur_row, self.cur_col));
                    };

                    if t.arr {
                        let mut vc = vec![];
                        for s in v.split('\n') {
                            match s.parse::<f64>() {
                                Ok(res) => {
                                    if range.check(res) != 0 {
                                        return Err(format!("key:{}, row:{} col:{} num range err:{}->{}->{}", key, self.cur_row, self.cur_col, v, s, res));
                                    }
                                    vc.push(Var::Num(res));
                                },
                                Err(e) => {
                                    return Err(format!("key:{}, row:{} col:{} num parse err:{}", key, self.cur_row, self.cur_col, e));
                                }
                            }
                        }
                        _value = Some(Var::List(vc));
                    } else {
                        match v.parse::<f64>() {
                            Ok(res) => {
                                if range.check(res) != 0 {
                                    return Err(format!("key:{}, row:{} col:{} num range err:{}", key, self.cur_row, self.cur_col, res));
                                }
                                _value = Some(Var::Num(res));
                            },
                            Err(e) => {
                                return Err(format!("key:{}, row:{} col:{} num parse err:{}", key, self.cur_row, self.cur_col, e));
                            }
                        }
                    }
                },
                "str" => {
                    if t.arr {
                        let mut vc = Vec::new();
                        for s in v.split('\n') {
                            vc.push(Var::Str(s.to_string()));
                        }
                        _value = Some(Var::List(vc));
                    } else {
                        _value = Some(Var::Str(v.clone()));
                    }
                },
                _ => {
                    continue;
                }
            }

            if let Some(res) = _value {
                if let Some(m) = group_map {
                    if m.contains_key(key) {
                        return Err(format!("key:{} row:{}, col:{} group:{} repeat", key, self.cur_row, self.cur_col, groupname));
                    }
                    m.insert(key.clone(), res);
                } else {
                    if t.global {
                        if let Var::Map(m) = globals {
                            if m.contains_key(key) {
                                return Err(format!("key:{} row:{}, col:{} global repeat", key, self.cur_row, self.cur_col));
                            }
                            m.insert(key.clone(), res);
                        }
                    } else {
                        if let Var::Map(m) = &mut item {
                            m.insert(key.clone(), res);
                        }
                    }
                }
            };
        }

        for (k,v) in &mut groups {
            if v.is_empty() {
                continue;
            }

            if let Var::Map(m) = &mut item {
                if m.contains_key(k) {
                    return Err(format!("key:{} row:{}, col:{} group repeat", k, self.cur_row, self.cur_col));
                }
                m.insert(k.clone(), Var::Map(std::mem::replace(v, HashMap::new())));
            }
        }

        for (k,v) in &mut grouparrs {
            if v.is_empty() {
                continue;
            }

            let mut hasv = true;
            let mut pos = 0;
            let mut arr = vec![];

            while hasv {
                hasv = false;
                let mut map = HashMap::new();
                for (k1, v1) in v.iter_mut() {
                    if let Var::List(l) = v1 {
                        if l.len() > pos {
                            hasv = true;
                            map.insert(k1.clone(), std::mem::replace(&mut l[pos], Var::None));
                        }
                    }
                }
                pos += 1;
                if hasv {
                    arr.push(Var::Map(map));
                }
            }
            if let Var::Map(m) = &mut item {
                if m.contains_key(k) {
                    return Err(format!("key:{} row:{}, col:{} grouparr repeat", k, self.cur_row, self.cur_col));
                }
                m.insert(k.clone(), Var::List(arr));
            }
        }

        Ok(item)
    }

    fn parse_title(&mut self, line:&Vec<String>) -> Result<Vec<Title>, String> {
        let mut ts = Vec::new();
        let mut _valid = false;
        self.cur_col = 0;
        
        for (i, v) in line.iter().enumerate() {
            self.cur_col = i;

            let mut t = Title::new();

            let vs = v.split('\n');
            for s in vs {
                let vs1 = s.split(':');
                let vc:Vec<_> = vs1.collect();
                if vc.len() == 2 {
                    t.propertys.insert(vc[0].to_string(), vc[1].to_string());
                }
            }
            t.propertys.insert("name".to_string(), v.clone());
            if let Some(s) = self.parse_title_property(&mut t) {
                return Err(s);
            }
            // 所有的title都算有效吧，等后续解析type的时候，无效，无数据即可，不要报错了
            _valid = true;
            ts.push(t);
        }

        Ok(ts)
    }

    fn parse_title_property(&self, t:&mut Title) -> Option<String> {
        if !t.propertys.contains_key("key") {
            return  Option::None;
        }

        // 外面保证一定有name
        if !t.propertys.contains_key("name") {
            return Option::None;
        }

        let type_ = match t.propertys.get("type") {
            Some(r) => r,
            None => {
                t.propertys.insert("type".to_string(), "num".to_string());
                if let Some(r) = t.propertys.get("type") {
                    r
                } else {
                    return  Option::None;
                }
            }
        };

        if type_ == "num" {
            let range_s = t.propertys.entry("num".to_string()).or_insert("[|]".to_string());
            if range_s.len() < 3 {
                if let Some(name) = t.propertys.get("name") {
                    return Some(format!("{} range fmt error row:{} col:{}", name, self.cur_row, self.cur_col));
                } else {
                    return Some(format!("range fmt error row:{} col:{}", self.cur_row, self.cur_col));
                }
            }

            let l = range_s.as_bytes()[0];
            let r = range_s.as_bytes()[range_s.len() - 1];
            
            let mut range_t = RangeCheck::new();
            range_t.lcose = match l as char {
                '[' => true,
                '(' => false,
                _ => {
                    // 格式错误
                    if let Some(name) = t.propertys.get("name") {
                        return Some(format!("{} range fmt error row:{} col:{}", name, self.cur_row, self.cur_col));
                    } else {
                        return Some(format!("range fmt error row:{} col:{}", self.cur_row, self.cur_col));
                    }
                }
            };
            range_t.rclose = match r as char {
                ']' => true,
                ')' => false,
                _ => {
                    // 格式错误
                    if let Some(name) = t.propertys.get("name") {
                        return Some(format!("{} range fmt error row:{} col:{}", name, self.cur_row, self.cur_col));
                    } else {
                        return Some(format!("range fmt error row:{} col:{}", self.cur_row, self.cur_col));
                    }
                }
            };
            range_t.l = f64::MIN;
            range_t.r = f64::MAX;
            match range_s.find('|') {
                Some(pos) => {
                    if let Ok(r) = range_s[0..pos].parse::<f64>() {
                        range_t.l = r;
                    }
                    
                    if let Ok(r) = range_s[pos+1..range_s.len()].parse::<f64>() {
                        range_t.r = r;
                    }
                },
                None => {
                    if let Some(name) = t.propertys.get("name") {
                        return Some(format!("{} range fmt error row:{} col:{}", name, self.cur_row, self.cur_col));
                    } else {
                        return Some(format!("range fmt error row:{} col:{}", self.cur_row, self.cur_col));
                    }
                }
            };

            t.range = Some(range_t);
        }

        if let Some(must_value) = t.propertys.get("must_value") {
            if let Ok(r) = must_value.parse::<i32>() {
                t.must_value = r != 0;
            }
        }

        if let Some(global) = t.propertys.get("global") {
            if let Ok(r) = global.parse::<i32>() {
                t.global = r != 0;
            }
        }
        
        if let Some(arr) = t.propertys.get("arr") {
            if let Ok(r) = arr.parse::<i32>() {
                t.arr = r != 0;
            }
        }

        if !t.global {
            if let Some(grouparr) = t.propertys.get("grouparr") {
                t.grouparr = grouparr.clone();
                t.arr = true;
            }

            if let Some(group) = t.propertys.get("group") {
                t.group = group.clone();
            }
        }

        return  Option::None;
    }

    fn csv_parse(&self, path : &str) -> Result<Vec<Vec<String>>, String> {
        let file = match File::open(path) {
            Ok(f) => f,
            Err(e) => {
                return Result::Err(format!("{} open {}", path, e));
            }
        };
        let mut buf_readers = BufReader::new(file);
        
        let mut left_dot = false;
        let mut cur = 0;
    
        let mut buf = Vec::new();
        let mut l = Vec::new();
        let mut last_dot_cur = 0;
        let mut last_dot_i = 0;
        let mut line = String::new();
        let mut result: Vec<Vec<String>> = Vec::new();
        loop {
            let sz = match buf_readers.read_until(b'\n', &mut  buf) {
                Ok(sz) => sz,
                Err(e) => {
                    return Result::Err(format!("{} read {}", path, e));
                }
            };
    
            if sz == 0 {
                break;
            }
    
            if buf.ends_with(&vec![b'\n']) {
                buf.pop();
            }
            if buf.ends_with(&vec![b'\r']) {
                buf.pop();
            }
    
            cur = cur + 1;
            let mut last_c = b'0';
            for (idx, &v) in buf.iter().enumerate() {
                match v {
                    b'"' => {
                        if !left_dot {
                            left_dot = true;
                            last_dot_cur = cur;
                            last_dot_i = idx;
                        } else {
                            l.push(std::mem::replace(&mut line, String::new()));
                            left_dot = false;
                            last_dot_cur = 0;
                            last_dot_i = 0;
                        }
                    },
                    b',' => {
                        if left_dot {
                            return Result::Err(format!("fmt err dot no close {} row:{} col:{}", line, cur, idx));
                        }
                        if last_c != b'"' {
                            l.push(std::mem::replace(&mut line, String::new()));
                            left_dot = false;
                            last_dot_cur = 0;
                            last_dot_i = 0;
                        }
                    },
                    _ => {
                        line.push(v as char);
                    }
                }
                last_c = v;
            }
    
            if !line.is_empty() && !buf.is_empty() {
                line.push('\n');
            }
            if !left_dot && !l.is_empty() {
                result.push(std::mem::replace(&mut l, vec![]));
            }
    
            buf.clear();
        };
    
        if left_dot {
            return  Result::Err(format!("fmt err, dot err row:{} col:{}", last_dot_cur, last_dot_i));
        }
        if !l.is_empty() {
            result.push(std::mem::replace(&mut l, vec![]));
        }
        return Result::Ok(result);
    }
}

fn main() {
    let mut dir = match std::env::current_exe() {
        Ok(exe_path) => {
            match exe_path.parent() {
                Some(parrent_path) => {
                    match parrent_path.to_str() {
                        Some(dir) => {
                            String::from(dir)
                        },
                        None => {
                            print!("exe parrent dir str empty!");
                            return;
                        }
                    }
                },
                None => {
                    println!("exe parrent empty");
                    return;
                }
            }
        },
        Err(e) => {
            println!("{}", e);
            return;
        }
    };

    dir.push_str("./config_test.csv");
    let mut csvconfig = CsvConfig::new();
    match csvconfig.parse(&dir) {
        Ok(r) => {
            println!("{:?}", r);
        },
        Err(s) => {
            println!("{}", s);
        }
    };
}
