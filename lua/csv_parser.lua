-- 分析真实的一行数据,支持中间包含换行
local function p1(tbl, it, cur)
    local tbl_c = {}
    local left_dot = false
    local last_dot_i = 0
    local parse_ = function (str)
        local last_c = ""
        local len = string.len(str)
        for i = 1, len do
            local c = string.sub(str, i, i)
            if c == [["]] then
                if not left_dot then
                    left_dot = true
                    last_dot_i = i
                else
                    -- 闭合
                    local one = table.concat(tbl_c)
                    table.insert(tbl, one)
                    tbl_c = {}
                    left_dot = false
                end
            elseif c == [[,]] then
                -- 逗号的时候，一定是要闭合状态的
                assert(not left_dot, string.format("fmt err dot no close (%s), col:%d,row:%d", str, cur, i))
                if last_c ~= [["]] then
                    -- 如果上个字符是 " 说明已经闭合过了
                    local one = table.concat(tbl_c)
                    table.insert(tbl, one)
                    tbl_c = {}
                end
            else
                table.insert(tbl_c, c)
            end

            last_c = c
        end
        if #tbl_c > 0 then
            table.insert(tbl_c, "\n")
        end
        return not left_dot -- 解析完成了
    end
    local eof = false
    local str = ""
    while true do
        str = it()
        if not str then
            eof = true
            break
        end
        cur = cur + 1
        if parse_(str) then
            break
        end
    end
    assert(not left_dot, string.format("fmt err cur:%d,lastdot:%d", cur, last_dot_i))
    if #tbl_c > 0 then
        -- 最后一个field 未必有 ,
        -- 最后一个field 最后会有一个换行符，把它干掉
        local one = table.concat(tbl_c, "", 1, #tbl_c-1)
        table.insert(tbl, one)
        tbl_c = {}
    end
    return eof,cur
end

local csv_parser = {}

function csv_parser.p_file(file)
    local it = io.lines(file)
    return csv_parser.p_iter(it)
end

function csv_parser.p_lines(lines)
    local cur = 0
    local it = function()
        cur = cur + 1
        if cur > #lines then
            return nil
        end
        return lines[cur]
    end
    return csv_parser.p_iter(it)
end

function csv_parser.p_iter(it)
    local tbl = {}
    local cur = 0
    while true do
        local line = {}
        local eof,newcur = p1(line, it, cur)
        if #line > 0 then
            table.insert(tbl, line)
        end
        if eof then
            break
        end
        cur = newcur
    end
    return tbl
end

return csv_parser