local csv_parser = require "lua.csv_parser"
local csv_config = require "lua.csv_config"
local json = require "lua.json"

local print_line_info = function(info)
    for line,v in ipairs(info) do
        print("line", line)
        for _,f in ipairs(v) do
            print("#")
            print(f)
        end
    end
end

local function test_parser()
    print_line_info(csv_parser.p_file("test1.csv"))

    local lines = {}
    for v in io.lines("test1.csv") do
        table.insert(lines, v)
    end
    print_line_info(csv_parser.p_lines(lines))
    
    print_line_info(csv_parser.p_iter(io.lines("test1.csv"))) 
end

local function test1_config()
    local t = csv_config.new("test1.csv")
    print(t)
end

local function test2_config(csvpath, jsonpath)
    print("parse ", csvpath)
    local t = csv_config.new(csvpath)
    local s = json.encode(t)
    local f = io.open(jsonpath, "w")
    f:write(s)
    f:close()
end

test2_config(arg[1], arg[2])
