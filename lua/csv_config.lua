local parser = require "lua.csv_parser"

local csv_config = {}

function csv_config.new(path)
    local m = {}

    local line_infos = parser.p_file(path)
    local titles = {}
    local globals = {}
    local cur_title = nil
    local cur_titlename = nil
    local cur_data = {}

    for i,line in ipairs(line_infos) do
        local title_name = line[1]
        if string.len(title_name) > 0 then
            -- 新的一套配置
            if cur_titlename then
                titles[csv_config.title_name_line(cur_titlename)] = cur_data
            end
            
            cur_titlename = title_name
            local valid = true
            cur_title,valid = csv_config.parse_title(line)
            cur_data = {}

            assert(valid, string.format("title line:%d fmt error", i))
        else
            if cur_title then
                local item = csv_config.parse_data_line(cur_title, line, i, globals)
                if next(item) then
                    cur_data = csv_config.title_item_check(cur_data, cur_title, item)
                end
            end
        end
    end
    -- 通过标题栏的属性，将每一行的数据进行格式化
    if cur_titlename then
        titles[csv_config.title_name_line(cur_titlename)] = cur_data
    end

    for k,v in pairs(titles) do
        m[k] = v
    end

    for k,v in pairs(globals) do
        assert(not m[k], string.format("key:%s global repeat", k))
        m[k] = v
    end
    return m
end

function csv_config.title_name_line(title_name)
    local tbl = csv_config.split(title_name, "\n") or {[1] = title_name}
    return tbl[1]
end

function csv_config.title_item_check(cur_data, title, item)
    -- check default 值
    for i,v in ipairs(title) do
        if v.default then
            if v.type == "num" then
                item[v.key] = item[v.key] or tonumber(v.default)
            elseif v.type == "str" then
                item[v.key] = item[v.key]
            end
        end
    end

    local tag = title[1]

    -- check map
    if tag.type == "map_entry" then
        local entry_key = title[2]
        cur_data[item[entry_key.key]] = item
    elseif tag.type == "map" then
        cur_data = item
    else
        table.insert(cur_data, item) 
    end
    return cur_data
end

function csv_config.split(input, delimiter, cb)
    input = tostring(input)
    delimiter = tostring(delimiter)
    if (delimiter=='') then return false end
    local pos = 0
    local arr = cb or {}
    -- for each divider found
    for st,sp in function() return string.find(input, delimiter, pos, true) end do
        if cb then
            cb(string.sub(input, pos, st - 1))
        else
            table.insert(arr, string.sub(input, pos, st - 1))
        end
        pos = sp + 1
    end
    if cb then
        cb(string.sub(input, pos))
    else
        table.insert(arr, string.sub(input, pos))
    end
    return arr
end

function csv_config.trim(input)
    input = string.gsub(input, "^[ \t\n\r]+", "")
    return string.gsub(input, "[ \t\n\r]+$", "")
end

function csv_config.parse_data_line(title, line, curline, globals)
    local item = {}
    local empty = true
    local err = nil

    local set_kv = function(tbl, k,v)
        if tbl[k] then
            return false
        end
        tbl[k] = v
        return true
    end

    local groups = {}
    local grouparrs = {}
    for idx,v in ipairs(line) do
        local t = title[idx]
        if t.key then
            -- 如果是带group属性的，需要把值先存下来
            local group_set = nil
            if type(t.grouparr) == "string" then
                grouparrs[t.grouparr] = grouparrs[t.grouparr] or {}  
                group_set = function(group_item)
                    grouparrs[t.grouparr][t.key] = group_item
                end
            elseif type(t.group) == "string" then
                groups[t.group] = groups[t.group] or {}
                group_set = function(group_item)
                    groups[t.group][t.key] = group_item
                end
            end

            if string.len(v) == 0 then
                if t.must_value == 0 or t.default then
                    -- 可以为空就不管了
                else
                    err = string.format("key:%s, col:%d, row:%d value empty", t.key, curline, idx)
                end
            else
                local value = nil
                empty = false
                if t.type == "num" then
                    local parse_num = function(sn)
                        local n = tonumber(sn)
                        if type(n) ~= "number" then
                            err = string.format("key:%s, col:%d, row:%d val{%s} fmt err", t.key, curline, idx, sn)
                        elseif not t.range_check(n) then
                            err = string.format("key:%s, col:%d, row:%d range check err %s,%s", t.key, curline, idx, sn, t.range)
                        end
                        return n
                    end

                    local parse_arr_num = function()
                        local tbl = csv_config.split(v, "\n")
                        local item = {}
                        for i,sn in ipairs(tbl) do
                            table.insert(item, parse_num(sn))
                        end
                        return item
                    end

                    if t.arr == 1 then
                        value = parse_arr_num()
                    else
                        value = parse_num(v)
                    end
                elseif t.type == "str" then
                    if t.arr == 1 then
                        value = csv_config.split(v, "\n")
                    else
                        value = v
                    end
                end

                if value then
                    if group_set then
                        group_set(value)
                    else
                        if t.global == 1 then
                            assert(set_kv(globals, t.key, value), string.format("key:%s col:%d, row:%d global repeat", t.key, curline, idx))
                        else
                            assert(set_kv(item, t.key, value), string.format("key:%s col:%d, row:%d item repeat", t.key, curline, idx))
                        end
                    end
                end

                if err then
                    assert(false, err)
                end
            end
        end
    end

    -- groups 融合
    for name, v in pairs(groups) do
        if next(v) then
            item[name] = v 
        end
    end

    -- grouparr 融合
    for name, v in pairs(grouparrs) do
        local group = {}
        local hasv = true
        local idx = 0
        while hasv do
            hasv = false
            idx = idx + 1
            local gropuitem = {}
            for key, arr in pairs(v) do
                if arr[idx] then
                   hasv = true
                   gropuitem[key] = arr[idx]
                end
            end
            if hasv then
                table.insert(group, gropuitem)
            end
        end
        if next(group) then
            item[name] = group 
        end
    end
    return item
end

function csv_config.parse_title_field(s)
    local item = {}
    csv_config.split(s, "\n", function(p)
        local props = csv_config.split(p, ":")
        if #props == 2 then
            item[props[1]] = props[2]
        end
    end)
    return item
end

function csv_config.parse_title(line)
    local titles = {}
    local valid = false
    for i,v in ipairs(line) do
        local item = csv_config.parse_title_field(v)
        item["name"] = v
        
        local title = csv_config.title_props(item, i)
        table.insert(titles, title)
        if type(title.key) == "string" then
            valid = true
        end
    end

    local tag = titles[1]
    if tag.type == "map_entry" then
        local entry_key = titles[2]
        assert(entry_key, string.format("%s map_entry key nil", line[1]))
        assert(entry_key.type == "str", string.format("%s map_entry key type not str", line[1]))
    elseif tag.type == "entry" then
        -- 整个cur_data 就是一个map
    end
    return titles, valid
end

function csv_config.title_props(item, n)
    if not item.key then
        -- 非有效配置项目
        return item
    end

    if n == 1 then
        -- 是tag 不做任何解析
        item.type = item.type or "arr"
        return item
    end

    -- num,str
    item.type = item.type or "num"
    if item.type == "num" then
        -- range 默认是double的上下限制
        item.range = item.range or "[|]"
        local l = string.sub(item.range, 1, 1)
        local r = string.sub(item.range, string.len(item.range),string.len(item.range))
        
        local f_get_min_max = function()
            local p = string.find(item.range, "|")
            assert(type(p) == "number", string.format("title %s range error", item.name))
            local min = -math.huge
            local max = math.huge

            local min_str = string.sub(item.range, 2, p-1)
            if string.len(min_str) == 0 then
                -- 默认不配置就是负无穷
            else
                min = tonumber(min_str)
            end

            local max_str = string.sub(item.range, p+1, string.len(item.range) - 1)
            if string.len(max_str) == 0 then
                -- 默认不配置就是正无穷
            else
                max = tonumber(max_str)
            end
            assert(type(min) == "number" and type(max) == "number", string.format("title %s range:%s range error", item.name, item.range))
            return min,max
        end

        local min,max = f_get_min_max()
        item.min = min
        item.max = max
        local range_check_arr = {}
        if l == "[" then
            range_check_arr[1] = function(v)
                return min <= v
            end
        elseif l == "(" then
            range_check_arr[1] = function(v)
                return min < v
            end
        else
            assert(false, string.format("title %s range left error", item.name))
        end
        
        if r == "]" then
            range_check_arr[2] = function(v)
                return max >= v
            end
        elseif r == ")" then
            range_check_arr[2] = function(v)
                return max > v
            end
        else
            assert(false, string.format("title %s range right error", item.name))
        end

        item.range_check = function(v)
            return range_check_arr[1](v) and range_check_arr[2](v)
        end
    end

    if item.type ~= "num" and item.type ~= "str" then
        assert(false, string.format("field:%s type:%s err", item.key, item.type))
    end

    -- 必须有值
    item.must_value = tonumber(item.must_value) or 1

    -- 如果是global，默认可以无值
    item.global = tonumber(item.global) or 0
    if item.global == 1 then
        item.must_value = 0
    end

    item.arr = tonumber(item.arr) or 0
    if item.arr == 1 then
        item.must_value = 0
    end

    if item.grouparr then
        item.arr = 1
    end

    item.group = item.group

    return item
end
return csv_config