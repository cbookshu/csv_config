pip install -q -r ./utils/requirements.txt
@echo off
for /f %%i in ('dir /b/a *.xlsx*') do (
    set filename=%%~ni
)
@echo on
python.exe ./utils/export_csv.py %filename%.xlsx
pause