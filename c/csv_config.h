#ifndef _H_CSV_CONFIG_H_
#define _H_CSV_CONFIG_H_
#include "csv_str.h"

#ifdef __cplusplus
#define CSV_PARER_API   extern "C"
#else
#define CSV_PARER_API   
#endif

typedef struct csv_config_t csv_config_t;

CSV_PARER_API csv_config_t* csv_config_new(const char* path);
CSV_PARER_API void csv_config_delete(csv_config_t* c);

// map<str,var>
CSV_PARER_API csv_sl_t* csv_config_data(csv_config_t* c);
CSV_PARER_API const char* csv_config_err(csv_config_t* c);

#endif