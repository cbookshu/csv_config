#include "csv_parser.h"
#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <stdarg.h>
#include <errno.h>

/*
    区分三种获取每一行数据的方式：
    1. 从文件自己去读
    2. 传入数组
    3. 传入迭代器，获取下一行【lua或者cpp很方便，占用资源小，但是C的话原生是没有这个概念的】
*/

#define MALLOC_PER_COUNT    1024
struct csv_parser_t
{   
    void* ud;
    csv_parser_cb cb;
    char* err_msg;

    // private val
    FILE* fp;
};

static char* err_msg(const char* fmt, ...) {
    char* buf = malloc(1024);
    va_list args;
    va_start(args, fmt);
    vsprintf(buf, fmt, args);
    va_end(args);
    return buf;
}

static c_str read_line(FILE* fp) {
    size_t buff_sz = MALLOC_PER_COUNT;
    c_str str = csv_str_new("", 0);

    size_t r = 0;
    char c;
    char last_c = EOF;
    while (1)
    {
        if(feof(fp)) {
            break;
        }
        c = fgetc(fp);
        if(c == EOF) {
            break;
        }
        if(c == '\n') {
            if(r != 0 && last_c == '\r') {
                r--;
            }
            break;
        }
        last_c = c;
        str = csv_str_push(str, c);
        r++;
    }
    csv_str_resize(str, r);
    return str;
}

static c_str file_iter(struct csv_parser_t* p) {
    return read_line(p->fp);
}

CSV_PARER_API struct csv_parser_t *csv_parser_new(csv_parser_cb cb, void *ud)
{
    struct csv_parser_t* parser = calloc(1, sizeof(struct csv_parser_t));
    parser->cb = cb;
    parser->ud = ud;
    return parser;
}

CSV_PARER_API void csv_parser_delete(struct csv_parser_t *p)
{
    if(p->err_msg) {
        free(p->err_msg);
    }
    free(p);
}

CSV_PARER_API const char *csv_parser_errmsg(struct csv_parser_t *p)
{
    return p->err_msg;
}

CSV_PARER_API void *csv_parser_ud(struct csv_parser_t *p)
{
    return p->ud;
}

CSV_PARER_API int csv_parser_file(const char *file, struct csv_parser_t *p)
{
    FILE* fp = fopen(file, "rb");
    if(!fp) {
        p->err_msg = err_msg("open file:%s err:%d", file, errno);
        return 1;
    }
    // 私有变量用于适配iter
    p->fp = fp;

    int r =  csv_parser_iter(file_iter, p);

    fclose(fp);
    return r;
}

CSV_PARER_API int csv_parser_iter(csv_parser_iter_cb iter,struct csv_parser_t *p)
{
    char err = 0;
    c_str line = csv_str_new("", 0);
    char eof = 0;
    char left_dot = 0;
    size_t cur = 0;

    size_t last_dot_cur = 0;
    size_t last_dot_i = 0;
    
    csv_list_t *l = csv_list_new(32);
    while (1)
    {
        c_str str = iter(p);
        size_t sz = csv_str_size(str);
        if(sz > 0) {
            cur++;
            // 解析行
            char last_c = 0;

            for(size_t i = 0; i < sz; ++i) {
                char c = str[i];

                if(c == '"') {
                    if (!left_dot) {
                        left_dot = 1;

                        last_dot_cur = cur;
                        last_dot_i = i;
                    } else {
                        // line 成功闭合一次
                        csv_list_push(l, line);
                        line = csv_str_new("", 0);
                        left_dot = 0;
                        last_dot_cur = 0;
                        last_dot_i = 0;
                    }
                } else if(c == ',') {
                    if (left_dot) {
                        // err
                        err = 1;
                        p->err_msg = err_msg("fmt err dot no close (%s), col:%d,row:%d", line, cur, i);
                        csv_str_delete(str);
                        goto result;
                    }

                    if (last_c != '"') {
                        csv_list_push(l, line);
                        line = csv_str_new("", 0);
                        left_dot = 0;
                        last_dot_cur = 0;
                        last_dot_i = 0;
                    } 
                } else {
                    line = csv_str_push(line, c);
                }
                last_c = c;
            }

            if (csv_str_size(line) > 0) {
                line = csv_str_push(line, '\n');
            }
        }

        if (!left_dot&& csv_list_size(l) > 0) {
            p->cb(l, p);
            csv_list_delete_cb(l, csv_str_delete);
            l = csv_list_new(32);
        }

        csv_str_delete(str);

        if(sz == 0) {
            break;
        }
    }

    if (left_dot) {
        err = 1;
        p->err_msg = err_msg("fmt err, dot err col:%d,row:%d", last_dot_cur, last_dot_i);
        goto result;
    }

    if(csv_list_size(l) > 0) {
        p->cb(l, p);
        csv_list_delete_cb(l, csv_str_delete);
        l = NULL;
    }
result:
    csv_str_delete(line);
    csv_list_delete_cb(l, csv_str_delete);
    return err;
}
