#ifndef _H_CSV_PARSER_H_
#define _H_CSV_PARSER_H_
#include "csv_str.h"
#include <stddef.h>

#ifdef __cplusplus
#define CSV_PARER_API   extern "C"
#else
#define CSV_PARER_API   
#endif

struct csv_parser_t;

typedef void (* csv_parser_cb)(struct csv_list_t* line, struct csv_parser_t* p);
typedef c_str (* csv_parser_iter_cb)(struct csv_parser_t* p);

CSV_PARER_API struct csv_parser_t* csv_parser_new(csv_parser_cb cb, void* ud);
CSV_PARER_API void csv_parser_delete(struct csv_parser_t* p);
CSV_PARER_API const char* csv_parser_errmsg(struct csv_parser_t* p);
CSV_PARER_API void* csv_parser_ud(struct csv_parser_t* p);

// return: 0:ok,1:err->errmsg
CSV_PARER_API int csv_parser_file(const char* file, struct csv_parser_t* p);
CSV_PARER_API int csv_parser_iter(csv_parser_iter_cb iter, struct csv_parser_t* p);

#endif