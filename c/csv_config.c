#include "csv_config.h"
#include "csv_parser.h"
#include <stdlib.h>
#include <string.h>
#include <stdarg.h>
#include <math.h>
#include <float.h>

typedef struct csv_config_t {
	char* err_msg;
	csv_sl_t* items;
	csv_list_t* lines;
}csv_config_t;

typedef struct csv_range_check_t {
	char lclose;
	char rclose;
	double l;
	double r;
}csv_range_check_t;

typedef struct csv_title_t {
	csv_sl_t* propertys;	// 属性map<str,str>
	csv_range_check_t* range;	// 如果是num会带一个range check的函数
	int must_value;
	int global;
	c_str grouparr;
	c_str group;
	int arr;
}csv_title_t;

static char* err_msg(const char* fmt, ...) {
	char* buf = malloc(1024);
	va_list args;
	va_start(args, fmt);
	vsprintf(buf, fmt, args);
	va_end(args);
	return buf;
}

static int _double_cmp(double l, double r) {
	double tol = 0.0000001;	// 误差在6位小数
	double diff = l - r;
	if (fabs(diff) < tol) {
		return 0;
	}
	return diff;
}

static int _range_check_cb(csv_range_check_t* rc, double v) {
	int r = _double_cmp(v, rc->l);
	if (r < 0) {
		return -1;
	}
	if (!rc->lclose && r == 0) {
		return -1;
	}

	r = _double_cmp(v, rc->r);
	if (r > 0) {
		return 1;
	}
	if (!rc->rclose && r == 0) {
		return 1;
	}

	return 0;
}

static void _csv_var_print(csv_var_t* v) {
	switch (csv_var_type(v)) {
	case CSV_VAR_T_NUM:
		printf("{%lf}\r\n", *(double*)csv_var_cast(v));
		break;
	case CSV_VAR_T_STR:
	case CSV_VAR_T_CSTR:
		printf("{%s}\r\n", (c_str)csv_var_cast(v));
		break;
	case CSV_VAR_T_LIST:
		printf("{\r\n");
		csv_list_t* l = csv_var_cast(v);
		for (size_t i = 0; i < csv_list_size(l); ++i) {
			printf("[%lld]= {\r\n", i);
			_csv_var_print(csv_list_at(l, i));
			printf("}\r\n");
		}
		printf("}\r\n");
		break;
	case CSV_VAR_T_SORTLIST:
		printf("{\r\n");
		csv_sl_t* sl = csv_var_cast(v);
		for (csv_kv_entry_t* b = csv_sl_begin(sl);
			b != csv_sl_end(sl);
			++b) {
			printf("[%s]={\r\n", (c_str)b->key);
			_csv_var_print(b->val);
			printf("}\r\n");
		}
		printf("}\r\n");
		break;
	}
}

static void _parser_cb(struct csv_list_t* line, struct csv_parser_t* p) {
	csv_config_t* c = csv_parser_ud(p);

	//for (size_t i = 0; i < csv_list_size(line); ++i) {
	//	printf("#%s\n", (c_str)csv_list_at(line, i));
	//}
	csv_list_push(c->lines, csv_list_move(line));
}

static csv_list_t* _str_splite(c_str str, const char* d) {
	csv_list_t* l = csv_list_new(4);
	size_t n = strlen(d);
	const char* e = str + csv_str_size(str);
	const char* p = str;
	do
	{
		if ((e - p) < n) {
			break;
		}

		const char* f = strstr(p, d);
		if (!f) {
			break;
		}

		csv_list_push(l, csv_str_new(p, f - p));

		p = f + n;
	} while (p < e);

	if (p <= e) {
		csv_list_push(l, csv_str_new(p, e - p));
	}
	return l;
}

static c_str _str_sub(const char* str, size_t pos1, size_t pos2) {
	size_t l = strlen(str);
	pos2 = min(pos2, l-1);
	pos1 = max(0, pos1);
	if (pos1 < pos2) {
		const char* p = str + pos1;
		return csv_str_new(p, pos2 - pos1 + 1);
	}
	else {
		return csv_str_new("", 0);
	}
}

static int _str_2_double(const char* str, double *v) {
	char* p;
	*v = strtod(str, &p);
	if (p != str) {
		return 1;
	}
	return 0;
}

static void _sl_title_delete_cb(csv_title_t* title) {
	if (title->grouparr) {
		csv_str_delete(title->grouparr);
	}
	if (title->group) {
		csv_str_delete(title->group);
	}
	if (title->range) {
		free(title->range);
	}

	if (title->propertys) {
		csv_sl_delete_cb_v(title->propertys, csv_str_delete);
	}

	free(title);
}

static void _sl_list_sl_val_delete_cb(csv_sl_t* v) {
	csv_sl_delete_cb_v(v, csv_var_delete);
}

static csv_sl_t* _parse_title_field(c_str s) {
	csv_sl_t* item = csv_sl_new(4);
	csv_list_t* props = _str_splite(s, "\n");
	for (size_t i = 0; i < csv_list_size(props); ++i) {
		c_str p = csv_list_at(props, i);
		csv_list_t* prop = _str_splite(p, ":");
		if (csv_list_size(prop) == 2) {
			c_str k = csv_list_at(prop, 0);
			c_str v = csv_list_at(prop, 1);
			csv_sl_insert(item, k, v);
			// v 会直接传入使用，外部不再释放了
			csv_str_delete(k);
			csv_list_delete(prop);
		}
		else {
			csv_list_delete_cb(prop, csv_str_delete);
		}
	}
	csv_list_delete_cb(props, csv_str_delete);
	return item;
}

static int _title_props(csv_title_t* title, csv_config_t* c) {
	csv_sl_t* item = title->propertys;
	csv_kv_entry_t* key_item = csv_sl_get(item, "key");
	if (!key_item) {
		// 无效的item
		return 0;
	}

	csv_kv_entry_t* name_item = csv_sl_get(item, "name");

	csv_kv_entry_t* type_item = csv_sl_get(item, "type");
	if (!type_item) {
		type_item = csv_sl_insert(item,
			"type",
			csv_str_new("num", strlen("num")));
	}

	if (0 == c_str_cmp("num", type_item->val)) {
		c_str default_range = csv_str_new("[|]", strlen("[|]"));
		csv_kv_entry_t* range_item = csv_sl_get(item, "range");
		if (!range_item) {
			range_item = csv_sl_insert(item,
				"range",
				default_range
			);
			default_range = NULL;
		}

		c_str range = range_item->val;
		char l = range[0];
		char r = range[strlen(range) - 1];

		csv_range_check_t* range_check = calloc(1, sizeof(csv_range_check_t));
		range_check->lclose = l == '[' ? 1 : 0;
		range_check->rclose = l == ']' ? 1 : 0;
		range_check->l = -DBL_MAX;
		range_check->r = DBL_MAX;
		

		const char* p = strstr(range_item->val, "|");
		if (p == NULL) {
			free(range_check);
			if (default_range) {
				csv_str_delete(default_range);
			}
			c->err_msg = err_msg("title %s range %s error", name_item->val, range);
			return 0;
		}

		size_t pos = p - range_item->val;
		c_str l_str = _str_sub(range_item->val, 2, pos - 1);
		c_str r_str = _str_sub(range_item->val, pos + 1, strlen(range_item->val));
		if (strlen(l_str) != 0) {
			range_check->l = atof(l_str);
		}
		if (strlen(r_str) != 0) {
			range_check->r = atof(r_str);
		}
		
		title->range = range_check;

		csv_str_delete(l_str);
		csv_str_delete(r_str);
		if (default_range) {
			csv_str_delete(default_range);
		}
	}

	title->must_value = 1;
	csv_kv_entry_t* must_value_item = csv_sl_get(item, "must_value");
	if (must_value_item) {
		title->must_value = atoi(must_value_item->val);
	}

	title->global = 0;
	csv_kv_entry_t* global_item = csv_sl_get(item, "global");
	if(global_item) {
		title->global = atoi(global_item->val);
		if(title->global) {
			title->must_value = 0;
		}
	}

	title->arr = 0;
	csv_kv_entry_t* arr_item = csv_sl_get(item, "arr");
	if (arr_item) {
		title->arr = atoi(arr_item->val);
	}

	if(!title->global) {
		title->grouparr = NULL;
		csv_kv_entry_t* grouparr_item = csv_sl_get(item, "grouparr");
		if (grouparr_item) {
			title->grouparr = csv_str_dup(grouparr_item->val);
			title->arr = 1;
		}

		title->group = NULL;
		csv_kv_entry_t* group_item = csv_sl_get(item, "group");
		if (group_item) {
			title->group = csv_str_dup(group_item->val);
		}
	}
	return 1;
}

static csv_list_t* _parse_title(csv_list_t* line, csv_config_t* c, int* valid) {
	csv_list_t* titles = csv_list_new(32);

	*valid = 0;
	for (size_t i = 0; i < csv_list_size(line); ++i) {
		c_str v = csv_list_at(line, i);
		
		csv_title_t* title = calloc(1, sizeof(csv_title_t));

		title->propertys = _parse_title_field(v);
		csv_sl_insert(title->propertys,
			"name",
			csv_str_dup(v));

		if (_title_props(title, c)) {
			*valid = 1;
		}

		csv_list_push(titles, title);

		if (c->err_msg) {
			*valid = 0;
			return;
		}
	}

	return titles;
}

static csv_var_t* _parse_data_line(csv_list_t* title, csv_list_t* line, size_t curline, csv_sl_t* globals, csv_config_t*c) {
	// var_map<str, var>
	csv_var_t* var_item = csv_var_sortlist(csv_sl_new(16));
	csv_sl_t* item = csv_var_cast(var_item);
	// map<str,var_map<str,val>>
	csv_sl_t* group = csv_sl_new(4);
	csv_sl_t* grouparr = csv_sl_new(4);

	for (size_t i = 0; i < csv_list_size(line); ++i) {
		c_str v = csv_list_at(line, i);
		if(csv_list_size(title) <= i) {
			continue;
		}
		csv_title_t* t = csv_list_at(title, i);
		csv_kv_entry_t* key_entry = csv_sl_get(t->propertys, "key");
		csv_sl_t* group_sl = NULL;
		if (key_entry) {
			csv_sl_t* group_map = NULL;
			csv_sl_t* group_val = NULL;
			c_str key_str = key_entry->val;
			c_str groupname = NULL;
			csv_kv_entry_t* group_entry = csv_sl_get(t->propertys, "grouparr");
			if (group_entry) {
				group_map = grouparr;
				groupname = group_entry->val;
			}
			else {
				group_entry = csv_sl_get(t->propertys, "group");
				if (group_entry) {
					group_map = group;
					groupname = group_entry->val;
				}
			}
			if (group_entry) {
				csv_kv_entry_t* group_fields = csv_sl_get(group_map, group_entry->val);
				if (!group_fields) {
					// map<str,val>
					group_fields = csv_sl_insert(group_map,
						group_entry->val,
						csv_var_sortlist(csv_sl_new(4)));
					
				}
				group_val = csv_var_cast(group_fields->val);
			}

			if (strlen(v) == 0) {
				if (t->must_value != 0 && c->err_msg == NULL) {
					c->err_msg = err_msg("key:%s, col:%d row%d value empty", key_entry->val, curline, i);
				}
			}
			else {
				if (c->err_msg) {
					goto result;
				}
				csv_var_t* value = NULL;
				csv_kv_entry_t* type_entry = csv_sl_get(t->propertys, "type");
				if (c_str_cmp(type_entry->val, "num") == 0) {
					csv_kv_entry_t* range_entry = csv_sl_get(t->propertys, "range");
					if (t->arr == 1) {
						csv_list_t* vs = csv_list_new(8);
						csv_list_t* vlist = _str_splite(v, "\n");
						for (size_t j = 0; j < csv_list_size(vlist); ++j) {
							c_str sn = csv_list_at(vlist, j);
							double d;
							if (!_str_2_double(v, &d)) {
								c->err_msg = err_msg("key:%s col:%d row:%d val:%s fmt err",
									key_entry->val, curline, i, v, range_entry->val);
								csv_list_delete_cb(vlist, csv_str_delete);
								csv_list_delete_cb(vs, csv_var_delete);
								goto result;
							}
							if(!_range_check_cb(t->range, d)) {
								c->err_msg = err_msg("key:%s col:%d row:%d val:%s range err",
									key_entry->val, curline, i, v, range_entry->val);
								csv_list_delete_cb(vlist, csv_str_delete);
								csv_list_delete_cb(vs, csv_var_delete);
								goto result;
							}
							csv_list_push(vs, csv_var_num(d));
						}
						csv_list_delete_cb(vlist, csv_str_delete);
						value = csv_var_list(vs);
					}
					else {
						double d;
						if (!_str_2_double(v, &d)) {
							c->err_msg = err_msg("key:%s col:%d row:%d val:%s fmt err",
								key_entry->val, curline, i, v, range_entry->val);
							goto result;
						}
						if(!_range_check_cb(t->range, d)) {
							c->err_msg = err_msg("key:%s col:%d row:%d val:%s range err",
								key_entry->val, curline, i, v, range_entry->val);
							goto result;
						}
						value = csv_var_num(d);
					}
				}
				else {
					if (t->arr == 1) {
						csv_list_t* vs = csv_list_new(4);
						csv_list_t* l = _str_splite(v, "\n");
						for (size_t j = 0; j < csv_list_size(l); ++j) {
							c_str str = csv_list_at(l, j);
							// 这里直接将l 中的c_str 直接move
							csv_list_push(vs, csv_var_cstr(str));
						}
						// l中的c_str已经被move到vs中，不需要再额外释放
						csv_list_delete(l);
						value = csv_var_list(vs);
					}
					else {
						value = csv_var_str(v);
					}
				}

				if (value) {
					if (group_val) {
						if (csv_sl_get(group_val, key_entry->val)) {
							c->err_msg = err_msg("key:%s col:%d, row:%d group:%s repeat",
								key_entry->val, curline, i, groupname);
							csv_var_delete(value);
							goto result;
						}

						csv_sl_insert(group_val,
							key_entry->val,
							value);
					}
					else {
						if (t->global == 1) {
							if (csv_sl_get(globals, key_entry->val)) {
								c->err_msg = err_msg("key:%s col:%d, row:%d global repeat",
									key_entry->val, curline, i);
								csv_var_delete(value);
								goto result;
							}

							csv_sl_insert(globals,
								key_entry->val,
								value);
						}
						else {
							if (csv_sl_get(item, key_entry->val)) {
								c->err_msg = err_msg("key:%s col:%d, row:%d item repeat",
									key_entry->val, curline, i);
								csv_var_delete(value);
								goto result;
							}

							csv_sl_insert(item, 
								key_entry->val,
								value);
						}
					}
				}
			}
		}
	}

	if (c->err_msg) {
		free(c->err_msg);
		c->err_msg = NULL;
	}

	for (csv_kv_entry_t* b = csv_sl_begin(group);
		b != csv_sl_end(group);
		++b) {

		c_str name = b->key;

		// map<str, val>
		//csv_var_t* var_sl = csv_var_sortlist(csv_sl_new(4));

		csv_sl_t* value = csv_var_cast(b->val);
		if (csv_sl_size(value) > 0) {
			csv_kv_entry_t* e = csv_sl_get(item, name);
			if (e) {
				// 重复
				c->err_msg = err_msg("key:%s col:%d  item with group repeat",
					name, curline);
				//csv_var_delete(var_sl);
				goto result;
			}
			csv_sl_insert(item, 
				b->key,
				csv_var_grap(b->val));
		}
	}

	for (csv_kv_entry_t* b = csv_sl_begin(grouparr);
		b != csv_sl_end(grouparr);
		++b) {

		c_str name = b->key;

		// map<key,cs_list_t>
		csv_sl_t* value = csv_var_cast(b->val);

		if (csv_sl_size(value) > 0) {
			csv_kv_entry_t* e = csv_sl_get(item, name);
			if (e) {
				// 重复
				c->err_msg = err_msg("key:%s col:%d  item with grouparr repeat",
					name, curline);
				goto result;
			}

			int hasv = 1;
			size_t pos = 0;
			csv_var_t* vl = csv_var_list(csv_list_new(4));
			csv_list_t* l = csv_var_cast(vl);

			while (hasv) {
				hasv = 0;
				csv_var_t* var_one = csv_var_sortlist(csv_sl_new(4));
				csv_sl_t* one = csv_var_cast(var_one);
				for (csv_kv_entry_t* c = csv_sl_begin(value);
					c != csv_sl_end(value);
					++c) {
					csv_list_t* vs = csv_var_cast(c->val);

					if ((csv_list_size(vs) - 1) >= pos) {
						hasv = 1;
						csv_sl_insert(one,
							c->key,
							csv_var_grap(csv_list_at(vs, pos))
						);
					}
				}
				pos++;
				if (hasv) {
					csv_list_push(l, csv_var_grap(var_one));
				}
				csv_var_delete(var_one);
			}

			csv_sl_insert(item, 
				name,
				vl);
		}
	}

result:
	csv_sl_delete_cb_v(group, csv_var_delete);
	csv_sl_delete_cb_v(grouparr, csv_var_delete);

	return var_item;
}

CSV_PARER_API csv_config_t* csv_config_new(const char* path)
{
	size_t alloc_sz = sizeof(csv_config_t);
	csv_config_t* c = malloc(alloc_sz);
	c->err_msg = NULL;
	c->items = csv_sl_new(32);
	c->lines = csv_list_new(32);

	struct csv_parser_t* parser = csv_parser_new(_parser_cb, c);
	if (csv_parser_file(path, parser)) {
		char* errmsg = csv_parser_errmsg(parser);
		c->err_msg = csv_str_new(errmsg, strlen(errmsg));
		goto result;
	}

	// 指针引用，cur_titlename 本身所有的值都是lines中的，使用时会进行dup
	c_str cur_titlename = NULL;
	// list<csv_title_t>
	csv_list_t* cur_title = NULL;
	// map<str, list<map<str, csv_val_t>>> 
	csv_sl_t* titles = csv_sl_new(32);
	// map<str, csv_val_t>
	csv_sl_t* globals = csv_sl_new(8);
	// var_list<map<str,csv_val_t>>
	csv_var_t* cur_data = NULL;

	for (size_t i = 0; i < csv_list_size(c->lines); ++i) {
		csv_list_t* l = csv_list_at(c->lines, i);
		c_str title_name = csv_list_at(l, 0);
		if (csv_str_size(title_name) > 0) {
			printf("new title:%s\n", title_name);

			if (cur_titlename && cur_data) {
				csv_sl_insert(titles, cur_titlename, cur_data);
				cur_data = NULL;
			}
			else {
				if (cur_data) {
					csv_var_delete(cur_data);
					cur_data = NULL;
				}
			}

			if (cur_title) {
				csv_list_delete_cb(cur_title, _sl_title_delete_cb);
				cur_title = NULL;
			}
			int valid;
			cur_title = _parse_title(l, c, &valid);
			if (!cur_title || !valid) {
				if (!c->err_msg) {
					c->err_msg = err_msg("title line:%d fmt error", i + 1);
				}
				goto result;
			}

			cur_titlename = title_name;
			cur_data = csv_var_list(csv_list_new(32));
		}
		else {
			if (cur_title) {
				csv_var_t* item = _parse_data_line(cur_title, l, i, globals, c);
				if (csv_sl_size(csv_var_cast(item))) {
					csv_list_push(csv_var_cast(cur_data), item);
				}
				else {
					csv_var_delete(item);
				}
			}
		}
	}

	if (cur_titlename && cur_data) {
		csv_sl_insert(titles, cur_titlename, cur_data);
		cur_data = NULL;
	}

	for (csv_kv_entry_t* b = csv_sl_begin(titles);
		b != csv_sl_end(titles);
		++b) {

		csv_sl_insert(
			c->items,
			b->key,
			csv_var_grap(b->val)
		);
	}

	for (csv_kv_entry_t* b = csv_sl_begin(globals);
		b != csv_sl_end(globals);
		++b) {
		if (csv_sl_get(c->items, b->key)) {
			c->err_msg = err_msg("key:%s global and item repeat", b->key);
			goto result;
		}
		else {
			csv_sl_insert(c->items,
				b->key,
				csv_var_grap(b->val));
		}
	}

result:
	if (cur_title) {
		csv_list_delete_cb(cur_title, _sl_title_delete_cb);
	}
	if (cur_data) {
		csv_var_delete(cur_data);
	}
	csv_sl_delete_cb_v(titles, csv_var_delete);
	csv_sl_delete_cb_v(globals, csv_var_delete);
	csv_parser_delete(parser);
	return c;
}

CSV_PARER_API void csv_config_delete(csv_config_t* c)
{
	csv_sl_delete_cb_v(c->items, csv_var_delete);
	for (size_t i = 0; i < csv_list_size(c->lines); ++i) {
		csv_list_delete_cb(csv_list_at(c->lines, i), csv_str_delete);
	}
	csv_list_delete(c->lines);
	if (c->err_msg) {
		free(c->err_msg);
	}
	free(c);
}

CSV_PARER_API csv_sl_t* csv_config_data(csv_config_t* c)
{
	return c->items;
}

CSV_PARER_API const char* csv_config_err(csv_config_t* c)
{
	return c->err_msg;
}
