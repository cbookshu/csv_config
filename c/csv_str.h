#ifndef _H_CSV_STR_H_
#define _H_CSV_STR_H_
#include <stddef.h>

#ifdef __cplusplus
#define CSV_PARER_API   extern "C"
#else
#define CSV_PARER_API   
#endif

// 动态字符串
typedef char* c_str;

CSV_PARER_API c_str csv_str_new(const char*s, size_t n);
CSV_PARER_API c_str csv_str_dup(c_str str);
CSV_PARER_API void csv_str_delete(c_str str);
CSV_PARER_API size_t csv_str_size(c_str str);
CSV_PARER_API size_t csv_str_capcity(c_str str);
CSV_PARER_API c_str csv_str_append(c_str str, const char* s, size_t n);
CSV_PARER_API c_str csv_str_push(c_str str, char c);
CSV_PARER_API c_str csv_str_resize(c_str str, size_t sz);

// 动态数组
typedef struct csv_list_t  csv_list_t;
typedef void(*list_delete_cb)(void*);

CSV_PARER_API csv_list_t* csv_list_new(size_t capcity);
CSV_PARER_API void csv_list_delete(csv_list_t* l);
CSV_PARER_API void csv_list_delete_cb(csv_list_t* l, list_delete_cb cb);
CSV_PARER_API size_t csv_list_capcity(csv_list_t* l);
CSV_PARER_API size_t csv_list_size(csv_list_t* l);
CSV_PARER_API int csv_list_push(csv_list_t* l, void* p);
CSV_PARER_API void* csv_list_take(csv_list_t* l, size_t pos);
CSV_PARER_API void* csv_list_at(csv_list_t* l, size_t pos);
CSV_PARER_API csv_list_t* csv_list_move( csv_list_t* source);


// 平衡队列
typedef struct csv_sl_t csv_sl_t;
typedef struct csv_kv_entry_t {
    const char* key;
    void* val;
} csv_kv_entry_t;
typedef void(*sl_delete_cb)(csv_kv_entry_t*);
typedef void(*sl_delete_v_cb)(void*);

CSV_PARER_API csv_sl_t* csv_sl_new(size_t capcity);
CSV_PARER_API void csv_sl_delete(csv_sl_t* sl);
CSV_PARER_API void csv_sl_delete_cb(csv_sl_t* sl, sl_delete_cb cb);
CSV_PARER_API void csv_sl_delete_cb_v(csv_sl_t* sl, sl_delete_v_cb cb);
CSV_PARER_API csv_kv_entry_t* csv_sl_insert(csv_sl_t* sl, const char* key, void* p);
CSV_PARER_API csv_kv_entry_t* csv_sl_get(csv_sl_t* sl, const char* key);
CSV_PARER_API csv_kv_entry_t* csv_sl_take(csv_sl_t* sl, c_str key);
CSV_PARER_API csv_kv_entry_t* csv_sl_begin(csv_sl_t* sl);
CSV_PARER_API csv_kv_entry_t* csv_sl_end(csv_sl_t* sl);

CSV_PARER_API size_t csv_sl_size(csv_sl_t* sl);
CSV_PARER_API csv_kv_entry_t* csv_sl_at(csv_sl_t* sl, size_t pos);


// utils function
CSV_PARER_API int c_str_cmp(char* s1, char* s2);



// union with ref
typedef struct csv_var_t csv_var_t;
#define CSV_VAR_T_NUM           0
#define CSV_VAR_T_STR           1
#define CSV_VAR_T_CSTR          2
#define CSV_VAR_T_LIST          3
#define CSV_VAR_T_SORTLIST      4

CSV_PARER_API csv_var_t* csv_var_new(int t, void* p);
CSV_PARER_API void csv_var_delete(csv_var_t* var);
CSV_PARER_API void* csv_var_cast(csv_var_t* var);
CSV_PARER_API int csv_var_type(csv_var_t* var);
CSV_PARER_API csv_var_t* csv_var_grap(csv_var_t* var);

CSV_PARER_API csv_var_t* csv_var_num(double v);
CSV_PARER_API csv_var_t* csv_var_list(csv_list_t* l);
CSV_PARER_API csv_var_t* csv_var_str(const char* s);
CSV_PARER_API csv_var_t* csv_var_cstr(c_str str);
CSV_PARER_API csv_var_t* csv_var_sortlist(csv_sl_t* sl);


#endif