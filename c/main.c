#include "csv_parser.h"
#include "csv_str.h"
#include "csv_config.h"
#include <stdio.h>
#include <assert.h>
#ifdef _WIN32
#include <windows.h>
#include <crtdbg.h>
#include <malloc.h>
#else
#include <unistd.h> 
#endif
#include <stdlib.h>

static void cb(struct csv_list_t* line, struct csv_parser_t* p) {
    csv_list_t* ud = csv_parser_ud(p);

    csv_list_t* l = csv_list_move(line);
    csv_list_push(ud, l);
}

static void test_parse_csv() {
    csv_list_t* lines = csv_list_new(32);

    struct csv_parser_t* p = csv_parser_new(cb, lines);
    if (csv_parser_file("./config_test.csv", p)) {
        printf("%s\n", csv_parser_errmsg(p));
    }
    else {
        for (size_t i = 0; i < csv_list_size(lines); ++i) {
            csv_list_t* l = csv_list_at(lines, i);
            for (size_t j = 0; j < csv_list_size(l); ++j) {
                c_str str = csv_list_at(l, j);
                printf("#%s\n", str);
            }
        }
    }

    for (size_t i = 0; i < csv_list_size(lines); ++i) {
        csv_list_t* l = csv_list_at(lines, i);
        csv_list_delete_cb(l, csv_str_delete);
    }
    csv_list_delete(lines);
    csv_parser_delete(p);
}

static void test_str() {
    c_str str = csv_str_new("", 0);
    for (size_t i = 1; i < 128; ++i) {
        str = csv_str_push(str, i);
    }
    printf("%s\n", str);
    str = csv_str_append(str, str, strlen(str));
    printf("%s\n", str);
    str = csv_str_push(str, '\n');
    printf("%s", str);
    csv_str_delete(str);
}

static void test_list() {
    csv_list_t* l = csv_list_new(32);
    int n[] = { 1,2,3,4,5,6,7,8,9,0 };
    for (size_t i = 0; i < 10; ++i) {
        csv_list_push(l, n + i);
    }

    int* r = csv_list_take(l, 3);
    for (size_t i = 0; i < csv_list_size(l); ++i) {
        printf("%d\n", *(int*)csv_list_at(l, i));
    }

    r = csv_list_take(l, csv_list_size(l) - 1);
    for (size_t i = 0; i < csv_list_size(l); ++i) {
        printf("%d\n", *(int*)csv_list_at(l, i));
    }

    csv_list_delete(l);
}

static void test_sl() {
    int n[128] = {0};
    for (size_t i = 0; i < 128; ++i) {
        if (i != 64) {
            n[i] = i;
        }
    }

    csv_sl_t* sl = csv_sl_new(32);
    for (size_t i = 0; i < 128; ++i) {
        char buf[32] = { 0 };
        itoa(n[i], buf, 10);
        csv_sl_insert(sl, buf, n + i);
    }

    int add_n = 64;
    csv_kv_entry_t* entry = csv_sl_insert(sl, "64", &add_n);
    csv_kv_entry_t* entry1 = csv_sl_get(sl, "64");
    assert(entry == entry1);

    for (csv_kv_entry_t* b = csv_sl_begin(sl);
        b != csv_sl_end(sl); ++b) {
        printf("k:%s, v:%d\n", b->key, *(int*)b->val);
    }
    for (size_t i = 0; i < csv_sl_size(sl); ++i) {
        csv_kv_entry_t* b = csv_sl_at(sl, i);
        csv_str_delete(b->key);
    }
    csv_sl_delete(sl);
}

static void print_var_t(csv_var_t* v) {
    switch (csv_var_type(v)) {
    case CSV_VAR_T_NUM:
        printf("{%lf}", *(double*)csv_var_cast(v));
        break;
    case CSV_VAR_T_STR:
    case CSV_VAR_T_CSTR:
        printf("{%s}", (c_str)csv_var_cast(v));
        break;
    case CSV_VAR_T_LIST:
        csv_list_t* l = csv_var_cast(v);
        for (size_t i = 0; i < csv_list_size(l); ++i) {
            printf("[%lld]", i);
            print_var_t(csv_list_at(l, i));
            printf("\r\n");
        }
        break;
    case CSV_VAR_T_SORTLIST:
        csv_sl_t* sl = csv_var_cast(v);
        for (csv_kv_entry_t* b = csv_sl_begin(sl);
            b != csv_sl_end(sl);
            ++b) {
            printf("[%s]", (c_str)b->key);
            print_var_t(b->val);
            printf("\r\n");
        }
        break;
    }
}

static void test_config() {
    csv_config_t* c = csv_config_new("./config_test.csv");

    const char* err = csv_config_err(c);
    if (err) {
        printf("parse err:%s \r\n", err);
    }
    else {
        csv_sl_t* data = csv_config_data(c);
        for (csv_kv_entry_t* b = csv_sl_begin(data);
            b != csv_sl_end(data);
            ++b) {
            printf("[%s]", (c_str)b->key);
            print_var_t(b->val);
            printf("\r\n");
        }
    }

    csv_config_delete(c);
}

int main(int argc, char**argv) {
    char path[MAX_PATH] = {0};

#ifdef _WIN32
    // 获取当前控制台输出代码页编号  
    UINT outputCP = GetConsoleOutputCP();  
    // 设置控制台输出代码页为UTF-8  
    SetConsoleOutputCP(936);  
    // 获取当前控制台输入代码页编号  
    UINT inputCP = GetConsoleCP();  
    // 设置控制台输入代码页为UTF-8  
    SetConsoleCP(936);  
    GetCurrentDirectoryA(MAX_PATH, path);

    _CrtSetReportMode(_CRT_WARN, _CRTDBG_MODE_FILE);
    _CrtSetReportFile(_CRT_WARN, _CRTDBG_FILE_STDOUT);
    _CrtSetReportMode(_CRT_ERROR, _CRTDBG_MODE_FILE);
    _CrtSetReportFile(_CRT_ERROR, _CRTDBG_FILE_STDOUT);
    _CrtSetReportMode(_CRT_ASSERT, _CRTDBG_MODE_FILE);
    _CrtSetReportFile(_CRT_ASSERT, _CRTDBG_FILE_STDOUT);
#else
    getcwd(path, sizeof(path));
#endif
    
    test_config();

#ifdef _WIN32
    _CrtDumpMemoryLeaks();
#endif // _WIN32
    return 0;
}