#include "csv_str.h"
#include <stdlib.h>
#include <string.h>

typedef struct c_str_t {
    size_t n;
    size_t capcity;
    char s[0];
}c_str_t;

#define STR_CAPCITY 32
#define STR_HEAD_SIZE (sizeof(size_t) * 2)
#define CAST_STR_T(str) ((c_str_t*)(str - STR_HEAD_SIZE))

static size_t csv_str_space(c_str_t* str_t) {
    return str_t->capcity - str_t->n;
}
static const char* csv_str_end(c_str_t* str_t) {
    return str_t->s + str_t->n;
}

CSV_PARER_API c_str csv_str_new(const char *s, size_t n)
{
    size_t capcity = 0;
    if ((n + 1) >= STR_CAPCITY) {
        capcity = n + 1 + STR_CAPCITY;
    } else {
        capcity = STR_CAPCITY;
    }
    c_str_t* str_t = malloc(capcity + STR_HEAD_SIZE);
    str_t->n = n;
    str_t->capcity = capcity;
    memmove(str_t->s, s, n);
    str_t->s[n] = 0;
    return str_t->s;
}

CSV_PARER_API c_str csv_str_dup(c_str str)
{
    return csv_str_new(str, csv_str_size(str));
}

CSV_PARER_API void csv_str_delete(c_str str)
{
    if (!str) return;
    c_str_t* str_t = CAST_STR_T(str);
    free(str_t);
}

CSV_PARER_API size_t csv_str_size(c_str str)
{
    return CAST_STR_T(str)->n;
}

CSV_PARER_API size_t csv_str_capcity(c_str str)
{
    return CAST_STR_T(str)->capcity;
}

CSV_PARER_API c_str csv_str_append(c_str str, const char *s, size_t n)
{
    c_str_t* str_t = CAST_STR_T(str);
    size_t space = csv_str_space(str_t);
    if(space > n) {
        memmove(csv_str_end(str_t),s,n);
        str_t->n += n;
        str_t->s[str_t->n] = 0;
        return str_t->s;
    }

    size_t capcity = str_t->capcity + n + STR_CAPCITY;
    str_t = realloc(str_t, capcity + STR_HEAD_SIZE);
    memmove(csv_str_end(str_t), s, n);
    str_t->capcity = capcity;
    str_t->n += n;
    str_t->s[str_t->n] = 0;
    return str_t->s;
}

CSV_PARER_API c_str csv_str_push(c_str str, char c)
{
    c_str_t* str_t = CAST_STR_T(str);
    size_t space = csv_str_space(str_t);
    if(space > 1) {
        str_t->s[str_t->n] = c;
        str_t->n += 1;
        str_t->s[str_t->n] = 0;
        return str_t->s;
    }

    size_t capcity = str_t->capcity + 1 + STR_CAPCITY;
    str_t = realloc(str_t, capcity + STR_HEAD_SIZE);
    str_t->s[str_t->n] = c;
    str_t->n += 1;
    str_t->capcity = capcity;
    str_t->s[str_t->n] = 0;
    return str_t->s;
}

CSV_PARER_API c_str csv_str_resize(c_str str, size_t sz)
{
    c_str_t* str_t = CAST_STR_T(str);
    if (str_t->capcity > sz) {

        if (sz > str_t->n) {
            memset(str_t->s + str_t->n, 0, sz - str_t->n);
        }
        str_t->n = sz;
        str_t->s[sz] = 0;
        return str_t;
    }

    size_t capcity = sz + 1 + STR_CAPCITY;
    str_t = realloc(str_t, capcity + STR_HEAD_SIZE);
    memset(str_t->s + str_t->n, 0, sz - str_t->n);
    str_t->n = sz;
    str_t->capcity = capcity;
    str_t->s[sz] = 0;
    return str_t->s;
}

typedef struct csv_list_t {
    void** items;
    size_t n;
    size_t capcity;
} csv_list_t;

CSV_PARER_API csv_list_t *csv_list_new(size_t capcity)
{
    csv_list_t* l = malloc(sizeof(csv_list_t));
    l->n = 0;
    l->capcity = capcity;
    l->items = malloc(sizeof(void*) * capcity);
    return l;
}

CSV_PARER_API void csv_list_delete(csv_list_t *l)
{
    if (!l) return;
    free(l->items);
    free(l);
}

CSV_PARER_API void csv_list_delete_cb(csv_list_t* l, list_delete_cb cb)
{
    for (size_t i = 0; i < csv_list_size(l); ++i) {
        cb(csv_list_at(l, i));
    }
    csv_list_delete(l);
}

CSV_PARER_API size_t csv_list_capcity(csv_list_t *l)
{
    return l->capcity;
}

CSV_PARER_API size_t csv_list_size(csv_list_t *l)
{
    return l->n;
}

CSV_PARER_API int csv_list_push(csv_list_t *l, void* p)
{
    if(l->n >= l->capcity) {
        size_t capcity = l->capcity * 1.5;
        l->items = realloc(l->items, sizeof(void*) * capcity);
        l->capcity = capcity;
    }

    l->items[l->n] = p;
    l->n++;
    return 0;
}

CSV_PARER_API void* csv_list_take(csv_list_t *l, size_t pos)
{
    if(pos < 0 || pos > l->n) {
        return NULL;
    }
    c_str s = l->items[pos];
    if(pos == (l->n - 1)) {
        l->n--;
        return s;
    }
    memmove(l->items + pos, l->items + pos + 1, sizeof(void*) * (l->n - (pos + 1)));
    l->n--;
    return s;
}

CSV_PARER_API void* csv_list_at(csv_list_t *l, size_t pos)
{
    if(pos < 0 || pos > l->n) {
        return NULL;
    }
    return l->items[pos];
}

CSV_PARER_API csv_list_t* csv_list_move(csv_list_t *source)
{
    csv_list_t* dest = malloc(sizeof(csv_list_t));
    dest->capcity = source->capcity;
    dest->items = source->items;
    dest->n = source->n;

    source->items = malloc(sizeof(void*) * source->capcity);
    source->n = 0;
    return dest;
}

typedef struct csv_sl_t {
    csv_kv_entry_t* entry;
    size_t n;
    size_t capcity;
} csv_sl_t;

CSV_PARER_API csv_sl_t *csv_sl_new(size_t capcity)
{
    csv_sl_t* sl = malloc(sizeof(csv_sl_t));
    sl->n = 0;
    sl->entry = malloc(sizeof(csv_kv_entry_t) * capcity);
    sl->capcity = capcity;
    return sl;
}

CSV_PARER_API void csv_sl_delete(csv_sl_t *sl)
{
    if (!sl) return;
    free(sl->entry);
    free(sl);
}

CSV_PARER_API void csv_sl_delete_cb(csv_sl_t* sl, sl_delete_cb cb)
{
    for (csv_kv_entry_t* b = csv_sl_begin(sl);
        b != csv_sl_end(sl);
        ++b) {
        cb(b);
    }
    csv_sl_delete(sl);
}

CSV_PARER_API void csv_sl_delete_cb_v(csv_sl_t* sl, sl_delete_v_cb cb)
{
    for (csv_kv_entry_t* b = csv_sl_begin(sl);
        b != csv_sl_end(sl);
        ++b) {
        free(b->key);
        cb(b->val);
    }
    csv_sl_delete(sl);
}

CSV_PARER_API int c_str_cmp(char* s1, char* s2) {
    size_t l_sz = strlen(s1);
    size_t r_sz = strlen(s2);
    int r = 0;
    if (l_sz > r_sz) {
        r = 1;
    }
    else if (l_sz < r_sz) {
        r = -1;
    }
    else {
        r = strncmp(s1, s2, l_sz);
    }
    return r;
}

CSV_PARER_API csv_kv_entry_t *csv_sl_insert(csv_sl_t *sl, const char* key, void *p)
{
    // 找到比key小的最大的值
    csv_kv_entry_t* cmp = NULL;
    csv_kv_entry_t* little = NULL;
    if (sl->n > 0) {
        int left = 0;
        int right = sl->n - 1;
        while (left <= right) {
            int mid = left + (right - left) / 2;

            int r = c_str_cmp(sl->entry[mid].key, key);

            if (r == 0) {
                cmp = sl->entry + mid;
                break;
            }
            else if (r < 0) {
                little = sl->entry + mid;
                left = mid + 1;
            }
            else if (r > 0) {
                right = mid - 1;
            }
        }
    }
    if(cmp) {
        // 重复了！
        return NULL;
    }
    
    // 默认little 是NULL的时候，pos就是0，在开头插入
    size_t pos = 0;
    if(little) {
        // little下一个位置就是插入位置
        pos = little - sl->entry + 1;
    }
    // 好了要在pos位置插入,把[pos+1, end]往后挪
    if(sl->n >= sl->capcity) {
        sl->capcity *= 1.5;
        sl->entry = realloc(sl->entry, sizeof(csv_kv_entry_t) * sl->capcity);
    }

    if (sl->n > 0 
        && (pos <= (sl->n - 1))) {
        memmove(sl->entry + pos + 1, sl->entry + pos, sizeof(csv_kv_entry_t) * (sl->n - pos));
    }
    sl->n++;

    sl->entry[pos].key = strdup(key);
    sl->entry[pos].val = p;
    return sl->entry + pos;
}

CSV_PARER_API csv_kv_entry_t *csv_sl_get(csv_sl_t *sl, const char* key)
{
    if (sl->n == 0) {
        return NULL;
    }
    int left = 0;
    int right = sl->n - 1;

    while(left <= right) {
        int mid = left + (right - left) / 2;
        int r = c_str_cmp(sl->entry[mid].key, key);
        if (r == 0) {
            return sl->entry + mid;
        } else if (r < 0) {
            left = mid + 1;
        } else if (r > 0) {
            right = mid - 1;
        }
    }
    return NULL;
}

CSV_PARER_API csv_kv_entry_t *csv_sl_take(csv_sl_t *sl, c_str key)
{
    int left = 0;
    int right = sl->n - 1;

    csv_kv_entry_t* p = NULL;
    size_t pos = 0;
    while(left <= right) {
        int mid = left + (right - left) / 2;
        int r = c_str_cmp(sl->entry[mid].key, key);
        if (r == 0) {
            pos = mid;
            p = sl->entry + mid;
        } else if (r < 0) {
            left = mid + 1;
        } else if (r > 0) {
            right = mid - 1;
        }
    }

    if(!p) return NULL;

    if(pos < (sl->n - 1)) {
        memmove(sl->entry + pos, sl->entry + pos + 1, sizeof(csv_kv_entry_t) * (sl->n - pos + 1));
    }
    sl->n--;
    return p;
}

CSV_PARER_API csv_kv_entry_t *csv_sl_begin(csv_sl_t *sl)
{
    return sl->entry;
}

CSV_PARER_API csv_kv_entry_t *csv_sl_end(csv_sl_t *sl)
{
    return sl->entry + sl->n;
}

CSV_PARER_API size_t csv_sl_size(csv_sl_t *sl)
{
    return sl->n;
}

CSV_PARER_API csv_kv_entry_t *csv_sl_at(csv_sl_t *sl, size_t pos)
{
    return sl->entry + pos;
}


typedef struct csv_var_t {
    union {
        double v;
        c_str s;
        csv_list_t* l;
        csv_sl_t* sl;
    };
    int t;
    int ref;
}csv_var_t;
CSV_PARER_API csv_var_t* csv_var_new(int t, void* p)
{
    csv_var_t* v = calloc(1, sizeof(csv_var_t));
    v->t = t;
    v->ref = 1;

    switch (v->t)
    {
    case CSV_VAR_T_NUM:
        v->v = *(double*)p;
        break;
    case CSV_VAR_T_STR:
        v->s = csv_str_new(p, strlen(p));
        break;
    case CSV_VAR_T_CSTR:
        v->s = p;
        break;
    case CSV_VAR_T_LIST:
        v->l = p;
        break;
    case CSV_VAR_T_SORTLIST:
        v->sl = p;
        break;
    default:
        break;
    }
    return v;
}

CSV_PARER_API void csv_var_delete(csv_var_t* v)
{
    v->ref--;
    if (v->ref > 0) {
        return;
    }
    switch (v->t)
    {
    case CSV_VAR_T_NUM:
        break;
    case CSV_VAR_T_STR:
    case CSV_VAR_T_CSTR:
        csv_str_delete(v->s);
        break;
    case CSV_VAR_T_LIST:
        csv_list_delete_cb(v->l, csv_var_delete);
        break;
    case CSV_VAR_T_SORTLIST:
        csv_sl_delete_cb_v(v->sl, csv_var_delete);
        break;
    default:
        break;
    }
    free(v);
}

CSV_PARER_API void* csv_var_cast(csv_var_t* v)
{
    switch (v->t)
    {
    case CSV_VAR_T_NUM:
        return &v->v;
    case CSV_VAR_T_CSTR:
    case CSV_VAR_T_STR:
        return v->s;
    case CSV_VAR_T_LIST:
        return v->l;
    case CSV_VAR_T_SORTLIST:
        return v->sl;
    default:
        break;
    }
    return v;
}

CSV_PARER_API int csv_var_type(csv_var_t* var)
{
    return var->t;
}

CSV_PARER_API csv_var_t* csv_var_grap(csv_var_t* var)
{
    var->ref++;
    return var;
}

CSV_PARER_API csv_var_t* csv_var_num(double v)
{
    return csv_var_new(CSV_VAR_T_NUM, &v);
}

CSV_PARER_API csv_var_t* csv_var_list(csv_list_t* l)
{
    return csv_var_new(CSV_VAR_T_LIST, l);
}

CSV_PARER_API csv_var_t* csv_var_str(const char* s)
{
    return csv_var_new(CSV_VAR_T_STR, s);
}

CSV_PARER_API csv_var_t* csv_var_cstr(c_str str)
{
    return csv_var_new(CSV_VAR_T_CSTR, str);
}

CSV_PARER_API csv_var_t* csv_var_sortlist(csv_sl_t* sl)
{
    return csv_var_new(CSV_VAR_T_SORTLIST, sl);
}
