
import sys
import os
import re
import pandas as pd


def excel2csv(excel_file):
    dir = os.path.basename(excel_file)
    dir, _ = os.path.splitext(dir)
    dir += "/"
    if not os.path.isdir(f"{dir}"):
        os.mkdir(f"{dir}")
    pass
    f =  pd.ExcelFile(excel_file)
    for sheet_name in f.sheet_names: 
        sheet_dir = f"{dir}{sheet_name}/"
        if not os.path.isdir(sheet_dir):
            os.mkdir(sheet_dir)
        pass
        sheet_name_filter = re.sub(r"\(.*?\)", "", sheet_name)

        df = pd.read_excel(excel_file, sheet_name=sheet_name, header=None)
        csvpath = f"{sheet_dir}{sheet_name_filter}.csv"
        df.to_csv(csvpath,encoding="GBK", index=False, header=None)
        
        jsonpath = f"{sheet_dir}{sheet_name_filter}.json"
        os.system(f"lua.exe ./lua/test.lua {csvpath} {jsonpath}")
        pass

    pass

if __name__ == "__main__":
    if not sys.argv[1]:
        print("export_csv.py csvfile")
        print("csvfile miss")
        pass
    excel2csv(sys.argv[1])
    pass