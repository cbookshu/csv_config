#include <iostream>
#include <exception>
#include <fstream>
#include <vector>
#include <string>
#include <cstdarg>
#include <algorithm>
#include <unordered_map>
#include <memory>
#include <sstream>

#ifdef _WIN32
#include <windows.h>
#include <crtdbg.h>
#include <malloc.h>
#else
#include <unistd.h> 
#endif
#include <stdlib.h>

class bad_parse : public std::exception {
public:
    bad_parse(const char* fmt, ...){
        char buf[1024] = {0};
        va_list args;
        va_start(args, fmt);
        int sz = vsprintf(buf, fmt, args);
        va_end(args);
        msg_.append(buf, sz);
    }
    const char* what() const override {
        return msg_.c_str();
    }
private:
    std::string msg_;
};

class csv_parser {
    std::fstream f_;
public:
    csv_parser(const char* path) {
        f_ = std::fstream(path);
    }
    ~csv_parser() {
    }

    auto parse_file() -> std::vector<std::vector<std::string>>  {
        std::string line;
        bool left_dot = false;
        size_t cur = 0;

        size_t last_dot_cur = 0;
        size_t last_dot_i = 0;
        
        std::vector<std::vector<std::string>> result;
        std::vector<std::string> l;
        std::string buf;
        while(std::getline(f_, buf)) {
            char last_c = 0;
            cur++;
            size_t idx = 0;
            std::for_each(buf.begin(), buf.end(), [&](char c){
                switch(c) {
                    case '"':
                        if(!left_dot) {
                            left_dot = true;
                            last_dot_cur = cur;
                            last_dot_i = idx;
                        } else {
                            l.emplace_back(std::move(line));
                            left_dot = false;
                            last_dot_cur = 0;
                            last_dot_i = 0;
                        }
                        break;
                    case ',':
                        if (left_dot) {
                            throw bad_parse("fmt err dot no close (%s), col:%d,row:%d", line.c_str(), cur, idx);
                        }
                        if (last_c != '"') {
                            l.emplace_back(std::move(line));
                            left_dot = false;
                            last_dot_cur = 0;
                            last_dot_i = 0;
                        } 
                        break;
                    default:
                        line.push_back(c);
                        break;
                }
                ++idx;
                last_c = c;
            });
            if(!line.empty() && !buf.empty()) {
                line.push_back('\n');
            }
            if(!left_dot && !l.empty()) {
                result.emplace_back(std::move(l));
            }
        }
        if (left_dot) {
            throw bad_parse("fmt err, dot err col:%d,row:%d", last_dot_cur, last_dot_i);
        }

        if(!l.empty()) {
            result.emplace_back(std::move(l));
        }
        return result;
    }
};

class csv_config {
    std::size_t cur_row_;
    std::size_t cur_col_;
public:
    // c++ 11不适用variant了，自定义结构
    enum var_type_t {
        var_type_non = -1,
        var_type_num = 0,
        var_type_str = 1,
        var_type_map = 2,
        var_type_arr = 3,
    };
    typedef struct var_t {
        union {
            double v;
            std::string* s;
            std::unordered_map<std::string, var_t>* m;
            std::vector<var_t>* l;
        };
        var_type_t t;  // 0 ~ 3: v,s,m,l
        var_t(var_type_t type):t(type) {
            switch(t) {
                case var_type_num:
                    break;
                case var_type_str:
                    s = new std::string();
                    break;
                case var_type_map:
                    m = new std::unordered_map<std::string, var_t>;
                    break;
                case var_type_arr:
                    l = new std::vector<var_t>;
                    break;
            }
        }
        var_t(double d) {
            t = var_type_num;
            v = d;
        }
        var_t(std::vector<var_t>&& v) {
            t = var_type_arr;
            l = new std::vector<var_t>(std::forward<std::vector<var_t>>(v));
        }
        var_t(std::string&& v) {
            t = var_type_str;
            s = new std::string(std::forward<std::string>(v));
        }
        var_t(std::unordered_map<std::string, var_t>&& v) {
            t = var_type_map;
            m = new std::unordered_map<std::string, var_t>(std::forward<std::unordered_map<std::string, var_t>>(v));
        }
        var_t(var_t&& other) {
            t = other.t;
            other.t = var_type_non;
            switch(t) {
                case var_type_num:
                    v = other.v;
                    break;
                case var_type_str:
                    s = other.s;
                    other.s = nullptr;
                    break;
                case var_type_map:
                    m = other.m;
                    other.m = nullptr;
                    break;
                case var_type_arr:
                    l = other.l;
                    other.l = nullptr;
                    break;
                default:
                    break;
            }
        }
        ~var_t() {
            switch(t) {
                case var_type_num:
                    break;
                case var_type_str:
                    delete s;
                    break;
                case var_type_map:
                    delete m;
                    break;
                case var_type_arr:
                    delete l;
                    break;
                default:
                    break;
            }
        }
    
        void print() {
            switch (t)
            {
            case var_type_num:
                std::cout << "{" << v << "}";
                break;
            case var_type_str:
                std::cout << "{" << s->c_str() << "}";
                break;
            case var_type_map:
                for(auto& it:*m) {
                    std::cout << "{" << it.first << ":";
                    it.second.print();
                    std::cout << "}" << std::endl;
                }
                break;
            case var_type_arr:
                for(auto i = 0; i < l->size(); ++i) {
                    std::cout << "{" << i << ":";
                    l->at(i).print();
                    std::cout << "}" << std::endl;
                }
                break;   
            default:
                break;
            }
        }
    }var_t;

    typedef struct range_check_t {
        char lclose;
        char rclose;
        double l;
        double r;
    }range_check_t;

    typedef struct title_t {
        std::unordered_map<std::string,std::string> propertys;	// 属性
        std::unique_ptr<range_check_t> range;   // range 对于num进行范围判断
        int must_value;
        int global;
        std::string grouparr;
        std::string group;
        int arr;
    }title_t;

    auto parse_file(const char* path) -> var_t {
        csv_parser p(path);
        auto result = p.parse_file();

        var_t res(var_type_map);
        std::vector<title_t> cur_title;
        std::string cur_titlename;
        std::unique_ptr<var_t> cur_data;
        std::unordered_map<std::string, var_t> globals;
        for(auto& line: result) {
            cur_row_++;

            // line 是',' 分割好的一行数据的列表
            if(line.empty()) {
                continue;
            }
        
            auto& title_name = line.front();
            if(!title_name.empty()) {
                // 新的一个title
                std::cout << "new title:" << title_name << std::endl;
                if(!cur_titlename.empty() && cur_data) {
                    res.m->insert({std::move(cur_titlename), std::move(*cur_data)});
                    cur_data.reset();
                }
                cur_title = parse_title(line);
                cur_titlename = title_name;
                cur_data.reset(new var_t(var_type_arr));
            } else {
                if(!cur_title.empty()) {
                    auto item = parse_data_line(cur_title, line, globals);
                    if(item.t == var_type_map && !item.m->empty()) {
                        cur_data->l->emplace_back(std::move(item));
                    }
                }
            }
        }

        if(!cur_titlename.empty() && cur_data) {
            res.m->insert({std::move(cur_titlename), std::move(*cur_data)});
            cur_data.reset();
        }

        for(auto& it:globals) {
            auto it_insert = res.m->insert({
                it.first, std::move(it.second)
            });
            if(!it_insert.second) {
                throw bad_parse("key:%s global and item repeat", it.first.c_str());
            }
        }

        return res;
    }

    auto parse_data_line(std::vector<title_t>& titles, 
        std::vector<std::string>& l, 
        std::unordered_map<std::string, var_t>& globals) ->var_t {
        
        std::unordered_map<std::string,
            std::unordered_map<std::string, var_t>> group;
        std::unordered_map<std::string,
            std::unordered_map<std::string, var_t>> grouparr;
        auto item = var_t(var_type_map);
        std::unique_ptr<bad_parse> err;
        for(auto idx = 0; idx < l.size(); ++idx) {
            cur_col_ = idx;
            if(l.size() <= idx) {
                continue;
            }
            auto& v = l.at(idx);
            auto& t = titles.at(idx);
            auto it_key = t.propertys.find("key");
            if(it_key == t.propertys.end()) {
                continue;
            }
            std::unordered_map<std::string, var_t>* group_map = nullptr;
            std::string groupname;
            if(!t.grouparr.empty()) {
                group_map = &grouparr[t.grouparr];
                groupname = t.grouparr;
            } else if(!t.group.empty()) {
                group_map = &group[t.group];
                t.grouparr = t.group;
            }

            if(v.empty()) {
                if (t.must_value == 1) {
                    err.reset(new bad_parse("key:%s, row:%d col%d value empty", it_key->second.c_str(),cur_row_, cur_col_));
                }
                continue;
            }

            if(err) {
                throw *err;
            }

            auto& str_type = t.propertys["type"];
            auto& range = t.propertys["range"];
            std::unique_ptr<var_t> value;
            if(str_type == "num") {
                if(t.arr == 1) {
                    auto vs = split(v, '\n');
                    std::vector<var_t> arr_var;
                    for(auto& s:vs) {
                        double d;
                        if(!str_2_double(s.c_str(), &d)) {
                            throw bad_parse("key:%s row:%d col:%d val:%s fmt err",
									it_key->second.c_str(),cur_row_, cur_col_, v.c_str(), range.c_str());
                        }
                        if(range_check(t.range.get(), d) != 0) {
                            throw bad_parse("key:%s row:%d col:%d val:%s fmt err",
                                    it_key->second.c_str(),cur_row_, cur_col_, v.c_str(), range.c_str());
                        }
                        arr_var.push_back(var_t(d));
                    }
                    value.reset(new var_t(std::move(arr_var)));
                } else {
                    double d;
                    if(!str_2_double(v.c_str(), &d)) {
                        throw bad_parse("key:%s row:%d col:%d val:%s fmt err",
                                it_key->second.c_str(),cur_row_, cur_col_, v.c_str(), range.c_str());
                    }
                    if(range_check(t.range.get(), d) != 0) {
                        throw bad_parse("key:%s row:%d col:%d val:%s fmt err",
                                it_key->second.c_str(),cur_row_, cur_col_, v.c_str(), range.c_str());
                    }
                    value.reset(new var_t(d));
                }
            } else {
                if(t.arr == 1) {
                    auto vs = split(v, '\n');
                    std::vector<var_t> arr_var;
                    for(auto& s:vs) {
                        arr_var.push_back(var_t(std::move(s)));
                    }
                    value.reset(new var_t(std::move(arr_var)));
                } else {
                    auto tmps = v;
                    value.reset(new var_t(std::move(tmps)));
                }
            }
            if(value) {
                if(group_map) {
                    auto it_insert = group_map->insert({it_key->second, std::move(*value)});
                    if(!it_insert.second) {
                        throw bad_parse("key:%s row:%d, col:%d group:%s repeat",
									it_key->second.c_str(), cur_row_, cur_col_, groupname.c_str());
                    }
                } else {
                    if(t.global == 1) {
                        auto it_insert = globals.insert({it_key->second, std::move(*value)});
                        if(!it_insert.second) {
                            throw bad_parse("key:%s row:%d, col:%d global repeat",
									it_key->second.c_str(), cur_row_, cur_col_);
                        }
                    } else {
                        auto it_insert = item.m->insert({it_key->second, std::move(*value)});
                        if(!it_insert.second) {
                            throw bad_parse("key:%s row:%d, col:%d item repeat",
                                it_key->second.c_str(), cur_row_, cur_col_);
                        }
                    }
                }
            }
        }

        
        for(auto& it:group) {
            if(it.second.empty()) {
                continue;
            }
            auto it_insert = item.m->insert({
                it.first, std::move(it.second)
            });
            if(!it_insert.second) {
                throw bad_parse("key:%s col:%d  item with group repeat",
                it.first.c_str(), cur_row_);
            }
        }

        for(auto& it:grouparr) {
            if(it.second.empty()) {
                continue;
            }
            bool hasv = true;
            std::size_t idx = 0;
            var_t var_arr(var_type_arr);
            while(hasv) {
                hasv = false;
                var_t var(var_type_map);
                for(auto& it1:it.second) {
                    // it1.first:str
                    // it1.second: vector<var>
                    if(it1.second.l->size() > idx) {
                        hasv = true;
                        var.m->insert({it1.first, std::move(it1.second.l->at(idx))});
                    }
                }
                idx++;
                if(hasv) {
                    var_arr.l->push_back(std::move(var));
                }
            }
            auto it_insert = item.m->insert({
                it.first, std::move(var_arr)
            });
            if(!it_insert.second) {
                throw bad_parse("key:%s col:%d  item with grouparr repeat",
					it.first.c_str(), cur_row_);
            }
        }

        return item;
    }

    auto parse_title(std::vector<std::string>& l) -> std::vector<title_t> {
        std::vector<title_t> res;

        bool valid = false;
        cur_col_ = 0;
        for(auto& s:l) {
            cur_col_++;

            // 然后用 '\n' 分割
            // 再用 ':' 分割
            title_t t;
            auto s1 = split(s, '\n');
            std::for_each(s1.begin(), s1.end(), [&](std::string& a){
                auto s2 = split(a, ':');
                if(s2.size() == 2) {
                    t.propertys[std::move(s2[0])] = std::move(s2[1]);
                }
            });
            t.propertys["name"] = s;
            parse_title_property(t);

            res.emplace_back(std::move(t));
        }
        return res;
    }

    bool parse_title_property(title_t& t) {
        if(!t.propertys.count("key")) {
            return false;
        }

        auto& name = t.propertys["name"];
        auto it_type = t.propertys.find("type");
        if(it_type == t.propertys.end()) {
            it_type = t.propertys.insert({"type", "num"}).first;
        }

        if(it_type->second == "num") {
            auto it_range = t.propertys.find("range");
            if(it_range == t.propertys.end()) {
                it_range = t.propertys.insert({"range", "[|]"}).first;
            }
            if(it_range->second.size() < 3) {
                throw bad_parse("[%s] range fmt error row:%d col:%d", name.c_str(), cur_row_, cur_col_);
            }

            char l = it_range->second.front();
            char r = it_range->second.back();

            auto range = std::make_unique<range_check_t>();
            range->lclose = l == '[' ? 1 : 0;
            range->rclose = r == ']' ? 1 : 0;
            range->l = std::numeric_limits<double>::lowest();
#undef max
            range->r = std::numeric_limits<double>::max();

            auto pos = it_range->second.find("|");
            if(pos == std::string::npos) {
                throw bad_parse("[%s] range fmt error row:%d col:%d", name.c_str(), cur_row_, cur_col_);
            }

            auto lstr = it_range->second.substr(1, pos - 1);
            auto rstr = it_range->second.substr(pos+1);
            rstr.pop_back();

            if(!lstr.empty()) {
                range->l = std::atof(lstr.c_str());
            }
            if (!rstr.empty()) {
                range->r = std::atof(rstr.c_str());
            }
            
            t.range = std::move(range);
        }

        t.must_value = 1;
        auto it_must_value = t.propertys.find("must_value");
        if(it_must_value != t.propertys.end()) {
            t.must_value = std::atoi(it_must_value->second.c_str());
        }

        t.global = 0;
        auto it_global = t.propertys.find("global");
        if(it_global != t.propertys.end()) {
            t.global = std::atoi(it_global->second.c_str());
            if(t.global) {
                t.must_value = 0;
            }
        }

        t.arr = 0;
        auto it_arr = t.propertys.find("arr");
        if (it_arr != t.propertys.end()) {
            t.arr = std::atoi(it_arr->second.c_str());
        }

        if(!t.global) {
            auto it_grouparr = t.propertys.find("grouparr");
            if(it_grouparr != t.propertys.end()) {
                t.grouparr = it_grouparr->second;
                t.arr = 1;
            }

            auto it_group = t.propertys.find("group");
            if(it_group != t.propertys.end()) {
                t.group = it_group->second;
            }
        }
    }

    int range_check(range_check_t* rc, double v) {
        int r = _double_cmp(v, rc->l);
        if (r < 0) {
            return -1;
        }
        if (!rc->lclose && r == 0) {
            return -1;
        }

        r = _double_cmp(v, rc->r);
        if (r > 0) {
            return 1;
        }
        if (!rc->rclose && r == 0) {
            return 1;
        }
        return 0;
    }

    static int _double_cmp(double l, double r) {
        double tol = 0.0000001;
        double diff = l - r;
        if (std::fabs(diff) < tol) {
            return 0;
        }
        if(diff < 0) {
            return -1;
        }
        return 1;
    }

    bool str_2_double(const char* str, double *v) {
        char* p;
        *v = std::strtod(str, &p);
        if (p != str) {
            return true;
        }
        return false;
    }

    auto split(std::string& s, char c) -> std::vector<std::string> {
        std::stringstream ss(s);
        std::string line;
        std::vector<std::string> strs;
        while(std::getline(ss, line, c)) {
            strs.push_back(line);
        }
        return strs;
    };
};

int main(int argc, char**argv) {

#ifdef _WIN32
    // 获取当前控制台输出代码页编号  
    UINT outputCP = GetConsoleOutputCP();  
    // 设置控制台输出代码页为UTF-8  
    SetConsoleOutputCP(936);  
    // 获取当前控制台输入代码页编号  
    UINT inputCP = GetConsoleCP();  
    // 设置控制台输入代码页为UTF-8  
    SetConsoleCP(936);  

    _CrtSetReportMode(_CRT_WARN, _CRTDBG_MODE_FILE);
    _CrtSetReportFile(_CRT_WARN, _CRTDBG_FILE_STDOUT);
    _CrtSetReportMode(_CRT_ERROR, _CRTDBG_MODE_FILE);
    _CrtSetReportFile(_CRT_ERROR, _CRTDBG_FILE_STDOUT);
    _CrtSetReportMode(_CRT_ASSERT, _CRTDBG_MODE_FILE);
    _CrtSetReportFile(_CRT_ASSERT, _CRTDBG_FILE_STDOUT);
#endif
    try {
        csv_config c;
        auto result = c.parse_file("./config_test.csv");
        result.print();
    } catch(std::exception& e) {
        printf("bad: %s", e.what());
    }

    return 0;
}